//
//  ViewController.swift
//  Weather App
//
//  Created by Cesar Añasco on 26/10/18.
//  Copyright © 2018 Cesar Añasco. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func loginButtonPressed(_ sender: Any) {
        let username = usernameTextField.text!
        let password = passwordTextField.text!
        
        Auth.auth().signIn(withEmail: username, password: password){
            (data, error) in
            if let error = error {
                print(error)
                return
            }
            //print("Welcome...!!!")
            // Ejecuta la transicion, con el self se hace referencia a la clase principal
            self.performSegue(withIdentifier: "loginSegue", sender: self)
        }
    }
    
    //loginSegue
}

