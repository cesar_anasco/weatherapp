//
//  WeatherLoggedInViewController.swift
//  Weather App
//
//  Created by Cesar Añasco on 6/11/18.
//  Copyright © 2018 Cesar Añasco. All rights reserved.
//

import UIKit
import AlamofireObjectMapper
import Alamofire

class WeatherLoggedInViewController: UIViewController {

    @IBOutlet weak var cityTextFiels: UITextField!
    @IBOutlet weak var weatherLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func getWeatherButtonPressed(_ sender: Any) {
    let URL = "http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=baeb56a8de4d00b61fa198758e8ed08f"
        
  //  let URL = "http://api.openweathermap.org/data/2.5/weather?q=\
  //      (cityTextField.text ?? "quito")&appid=baeb56a8de4d00b61fa198758e8ed08f";
        

        Alamofire.request(URL).responseObject {(response:DataResponse<Weather>)
            in
            self.weatherLabel.text = response.value?.weather?[0].weather ?? ""
        }
    }

    @IBAction func goLogin(_ sender: Any) {
    dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goGeo(_ sender: Any) {
        self.performSegue(withIdentifier: "geoSegue", sender: self )
        cityTextFiels.text = ""
    }
    
}
