//
//  weather.swift
//  Weather App
//
//  Created by Cesar Añasco on 6/11/18.
//  Copyright © 2018 Cesar Añasco. All rights reserved.
//

import Foundation
//import ObjectMapper

//Sintaxis para agregar mas atributos

class Weather:Mappable {
    var weather: [SingleWeather]?
    var weatherid:Int?
    var name:String?
    
    required init?(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        weather <- map["weather"]
        weatherid <- map["id"]
        name <- map["name"]
    }
}
    
    class SingleWeather:Mappable {
        
        var weather:String?
        
        required init?(map: Map) {
        }
        
        func mapping(map: Map) {
            weather <- map["main"]
        }
    }

class weatherGeo: Mappable {
    
    var weather: [SingleWeather]?
    var weatherId: Int?
    var name: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        weather <- map["weather"]
        weatherId <- map["id"]
        name <- map["name"]
        
    }
    
}

